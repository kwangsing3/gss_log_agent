FROM centos:7
#MAINTAINER
LABEL org.opencontainers.image.authors="Jon_Chiu@genesis.com.tw"

WORKDIR /usr

#建立gss_log_agent資料夾供綁定
RUN mkdir /usr/local/gss_log_agent

RUN echo date && mv /etc/localtime /etc/localtime.backup1
# 設定 Taipei 時區
RUN ln -s /usr/share/zoneinfo/Asia/Taipei /etc/localtime && echo date && \
 localedef -c -f UTF-8 -i en_US en_US.UTF-8 && \
 echo 'LANG="en_US.UTF-8"'>/etc/locale.conf && source /etc/locale.conf

# 設定 perl環境
RUN yum install -y gzip gcc perl-CPAN 'perl(CGI)' 'perl(LWP::UserAgent)' \
perl-Socket6 perl-Test-Simple && \
yum clean all

RUN curl -L https://cpanmin.us | perl - App::cpanminus && \
cpan YAML < /dev/null && cpan install Term::ReadKey < /dev/null


#安裝Nodejs (暫時不限制版本) 及相關套件
RUN yum install -y epel-release
RUN yum install -y nodejs




#crontab功能因centOS映像預設不包含，所以"不設定"於Docker內部

# Other:
#docker_agent -> this images name
#docker run  -v /home/genesis/dockerfile_gssagent/gss_log_agent:/usr/local/gss_log_agent --restart=always -itd docker_agent 








