# GSS_Log_Agent

此為Docker的預設分支，詳細內容請至主branch [gss_log_agent](https://li1552-220.members.linode.com:3000/jon_chiu/gss_log_agent/src/branch/master)


```bash
#更新gss_log_agent模組
git submodule update --remote
```


# 安裝步驟
1.假設此專案安裝於: /home/genesis
```bash
git clone --recurse-submodules https://li1552-220.members.linode.com:3000/jon_chiu/gss_log_agent.git
```
```bash
cd gss_log_agent/gss_log_agent

npm install && tsc

cd  ../
```
2.從Dockfile 建立映像檔 gss_agent_images
```bash
docker build -t gss_agent_images . --no-cache
```
3.使用docker 安裝並綁定資料夾(須帶入絕對路徑) 
```bash
#--add-host=host.docker.internal:host-gateway 會去使用host的DNS
docker run --name gss_agent  -v /home/genesis/gss_log_agent/gss_log_agent:/usr/local/gss_log_agent --add-host=host.docker.internal:host-gateway --restart=always -itd gss_agent_images 
```

4.設定crontab 由外部進入docker內觸發
```bash
cp gss_log_agent/cronjob /etc/cron.d/gss_log_agent_cronjob
```

備註: 
```bash
#gss_agent_images建立時的參考映像
docker pull centos7 
```



